import React, { useState } from "react";
import Header from "./components/Header";
import Button from "./components/Button";
import Table from "./components/Table";
import Modal from "./components/Modal";
import "./App.css";

function App() {
  const mockDataTable = [
    {
      name: "Leslie Alexander",
      contact: "Contact Name Lorem.ipsum@lorem.com +65-78978987",
      accountType: "Enterprise",
      address: "Bukit Batok West Ave 9 Singapore",
      additionalData: null,
    },
    {
      name: "Leslie Alexander",
      contact: "Contact Name Lorem.ipsum@lorem.com +65-78978987",
      accountType: "Enterprise",
      address: "Bukit Batok West Ave 9 Singapore",
      additionalData: null,
    },
    {
      name: "Leslie Alexander",
      contact: "Contact Name Lorem.ipsum@lorem.com +65-78978987",
      accountType: "Enterprise",
      address: "Bukit Batok West Ave 9 Singapore",
      additionalData: null,
    },
  ];

  const mockDataListContactPerson = [
    {
      id: 1,
      listContactEmail: [{ id: 1 }],
      listContactPhone: [{ id: 1 }],
    },
  ];
  const mockDataListAddress = [
    {
      id: 1,
    },
  ];
  const [dataTable, setDataTable] = useState(mockDataTable);
  const [isShowModal, setIsShowModal] = useState(false);
  const [state, setState] = useState(null);
  const [listContactPerson, setListContactPerson] = useState(
    mockDataListContactPerson
  );
  const [listAddress, setListAddress] = useState(mockDataListAddress);

  const handleChange = (e) => {
    setState({
      ...state,
      [e.target.name]: e.target.value,
    });
  };

  const handleSubmit = () => {
    setDataTable([
      ...dataTable,
      {
        name: state.clientName,
        contact: `${state.contactName0} ${state.contactEmail0} ${state.contactPhone0}`,
        accountType: state.accountType,
        address: state.address0,
        additionalData: state,
      },
    ]);

    if (window.confirm("Success create data")) {
      setIsShowModal(false);
    }
  };

  console.log(dataTable);
  return (
    <>
      <div className="container-frame is-mobile">
        <Header />
        <div style={{ marginLeft: 24, marginRight: 24 }}>
          <Button
            onClick={() => setIsShowModal(true)}
            styleContainer={{ marginTop: 48 }}
            label="Add New Client"
          />
          <Table
            styleContainerTable={{ marginTop: 32 }}
            dataTable={dataTable}
          />
        </div>
        <Modal
          handleSubmit={handleSubmit}
          handleChange={handleChange}
          setState={setState}
          setListAddress={setListAddress}
          listAddress={listAddress}
          setListContactPerson={setListContactPerson}
          listContactPerson={listContactPerson}
          setIsShowModal={setIsShowModal}
          isShowModal={isShowModal}
        />
      </div>

      <p className="show-on-mobile hide">
        Warning this application can't view on mobile resolution, please back to
        dekstop resolution
      </p>
    </>
  );
}

export default App;
