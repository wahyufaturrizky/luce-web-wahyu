import React from "react";
import "./Table.css";

const Table = (props) => {
  return (
    <table style={props.styleContainerTable}>
      <thead>
        <tr>
          <th>Name</th>
          <th>Contact</th>
          <th>Account Type</th>
          <th>Address</th>
        </tr>
      </thead>
      <tbody>
        {props.dataTable.map((data, index) => {
          const { name, contact, accountType, address } = data;
          return (
            <tr key={index}>
              <td>{name}</td>
              <td>{contact}</td>
              <td>{accountType}</td>
              <td>{address}</td>
            </tr>
          );
        })}
      </tbody>
    </table>
  );
};

export default Table;
