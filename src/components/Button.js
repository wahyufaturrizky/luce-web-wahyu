import React from "react";
import "./Button.css";

const Button = (props) => {
  return (
    <button
      type={props.submit ? "submit" : undefined}
      onClick={props.onClick}
      style={props.styleContainer}
      className={
        props.secondary
          ? "container-button-secondary"
          : "container-button-primary"
      }
    >
      {props.label}
    </button>
  );
};

export default Button;
