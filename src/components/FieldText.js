import React from "react";
import "./FieldText.css";

const FieldText = (props) => {
  return (
    <div className="form-control">
      <label>{props.label}</label>
      <input
        onChange={props.onChange}
        required={props.required}
        type={props.fieldType}
        name={props.fieldName}
        placeholder={props.placeholder}
      />
    </div>
  );
};

export default FieldText;
