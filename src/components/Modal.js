import React from "react";
import FieldText from "./FieldText";
import FieldSelect from "./FieldSelect";
import "./Modal.css";
import Button from "./Button";

const Modal = (props) => {
  return (
    props.isShowModal && (
      <div className="shadow-container">
        <form onSubmit={props.handleSubmit}>
          <div className="modal-container modal-container-mobile">
            <p
              style={{
                fontWeight: 700,
                fontStyle: "normal",
                fontSize: 28,
                marginBottom: 48,
                marginLeft: 26,
              }}
            >
              Create New Client
            </p>

            <div className="row">
              <div className="col-6">
                <p
                  style={{ fontSize: 20, fontStyle: "normal", fontWeight: 400 }}
                >
                  Basic Information
                </p>
                <FieldText
                  required
                  onChange={(e) => props.handleChange(e)}
                  fieldType="text"
                  fieldName="clientName"
                  label="Client Name *"
                  placeholder="Enter client name"
                />
                <FieldSelect
                  onChange={(e) => props.handleChange(e)}
                  required
                  dataSource={[
                    {
                      label: "Enterprise",
                      value: "Enterprise",
                    },
                    {
                      label: "Individual",
                      value: "Individual",
                    },
                  ]}
                  fieldName="accountType"
                  label="Account Type *"
                />

                <div
                  style={{
                    display: "flex",
                    alignItems: "center",
                    justifyContent: "space-between",
                  }}
                >
                  <p
                    style={{
                      fontSize: 20,
                      fontStyle: "normal",
                      fontWeight: 400,
                    }}
                  >
                    Contact Person
                  </p>

                  <p
                    style={{
                      color: "#4EA8DC",
                      cursor: "pointer",
                      fontSize: 14,
                      fontWeight: "normal",
                    }}
                    onClick={() =>
                      props.listContactPerson.length > 4
                        ? window.alert("Can't more than four field")
                        : props.setListContactPerson([
                            ...props.listContactPerson,
                            {
                              id: props.listContactPerson.length + 1,
                              listContactEmail: [{ id: 1 }],
                              listContactPhone: [{ id: 1 }],
                            },
                          ])
                    }
                  >
                    Add Contact Person +
                  </p>
                </div>

                <p
                  style={{
                    fontSize: 16,
                    fontStyle: "normal",
                    fontWeight: "normal",
                    marginTop: 24,
                  }}
                >
                  Primary Contact
                </p>

                {props.listContactPerson.map((data, index) => {
                  return (
                    <div key={index}>
                      <FieldText
                        fieldType="text"
                        required
                        onChange={(e) => props.handleChange(e)}
                        fieldName={`contactName${index}`}
                        label="Name *"
                        placeholder="Enter contact name"
                      />

                      {data.listContactEmail.map((emailList, indexEmail) => (
                        <div
                          style={{
                            display: "flex",
                            justifyContent: "space-between",
                            alignItems: "center",
                          }}
                        >
                          <FieldText
                            fieldType="email"
                            onChange={(e) => props.handleChange(e)}
                            required
                            fieldName={`contactEmail${indexEmail}`}
                            label="Email *"
                            placeholder="Enter email address"
                          />

                          {emailList.id === 1 ? (
                            <p
                              style={{
                                color: "#4EA8DC",
                                cursor: "pointer",
                                fontSize: 14,
                                fontWeight: "normal",
                              }}
                              onClick={() => {
                                if (data.listContactEmail.length > 4) {
                                  window.alert("can't more than four");
                                } else {
                                  const newData = props.listContactPerson.map(
                                    (subdata) => {
                                      if (subdata.id === data.id) {
                                        return {
                                          ...subdata,
                                          listContactEmail: [
                                            ...subdata.listContactEmail,
                                            {
                                              id:
                                                subdata.listContactEmail
                                                  .length + 1,
                                            },
                                          ],
                                        };
                                      } else {
                                        return subdata;
                                      }
                                    }
                                  );

                                  props.setListContactPerson(newData);
                                }
                              }}
                            >
                              Add Other +
                            </p>
                          ) : (
                            <p
                              style={{
                                color: "#555555",
                                cursor: "pointer",
                                fontSize: 14,
                                fontWeight: "normal",
                              }}
                              onClick={() => {
                                const newData = props.listContactPerson.map(
                                  (subdata) => {
                                    if (subdata.id === data.id) {
                                      let remove =
                                        subdata.listContactEmail.filter(
                                          (filtering) =>
                                            filtering.id !== emailList.id
                                        );
                                      return {
                                        ...subdata,
                                        listContactEmail: remove,
                                      };
                                    } else {
                                      return subdata;
                                    }
                                  }
                                );

                                props.setListContactPerson(newData);
                              }}
                            >
                              🗑️
                            </p>
                          )}
                        </div>
                      ))}

                      {data.listContactPhone.map((phoneList, indexPhone) => (
                        <div
                          style={{
                            display: "flex",
                            justifyContent: "space-between",
                            alignItems: "center",
                          }}
                        >
                          <FieldText
                            fieldType="number"
                            onChange={(e) => props.handleChange(e)}
                            required
                            fieldName={`contactPhone${indexPhone}`}
                            label="Phone Number *"
                            placeholder="Enter Phone Number"
                          />

                          {phoneList.id === 1 ? (
                            <p
                              style={{
                                color: "#4EA8DC",
                                cursor: "pointer",
                                fontSize: 14,
                                fontWeight: "normal",
                              }}
                              onClick={() => {
                                if (data.listContactPhone.length > 4) {
                                  window.alert("can't more than four");
                                } else {
                                  const newData = props.listContactPerson.map(
                                    (subdata) => {
                                      if (subdata.id === data.id) {
                                        return {
                                          ...subdata,
                                          listContactPhone: [
                                            ...subdata.listContactPhone,
                                            {
                                              id:
                                                subdata.listContactPhone
                                                  .length + 1,
                                            },
                                          ],
                                        };
                                      } else {
                                        return subdata;
                                      }
                                    }
                                  );

                                  props.setListContactPerson(newData);
                                }
                              }}
                            >
                              Add Other +
                            </p>
                          ) : (
                            <p
                              style={{
                                color: "#555555",
                                cursor: "pointer",
                                fontSize: 14,
                                fontWeight: "normal",
                              }}
                              onClick={() => {
                                const newData = props.listContactPerson.map(
                                  (subdata) => {
                                    if (subdata.id === data.id) {
                                      let remove =
                                        subdata.listContactPhone.filter(
                                          (filtering) =>
                                            filtering.id !== phoneList.id
                                        );
                                      return {
                                        ...subdata,
                                        listContactPhone: remove,
                                      };
                                    } else {
                                      return subdata;
                                    }
                                  }
                                );

                                props.setListContactPerson(newData);
                              }}
                            >
                              🗑️
                            </p>
                          )}
                        </div>
                      ))}

                      <div style={{ border: "1px solid #F1F1F1" }}></div>
                      <p
                        onClick={() => {
                          if (props.listContactPerson.length !== 1) {
                            let romveListContact =
                              props.listContactPerson.filter(
                                (filtering) => filtering.id !== data.id
                              );
                            props.setListContactPerson(romveListContact);
                          }
                        }}
                        style={{
                          textAlign: "right",
                          marginTop: 8,
                          cursor: "pointer",
                        }}
                      >
                        X
                      </p>
                    </div>
                  );
                })}
              </div>

              <div className="col-6">
                <div
                  style={{
                    display: "flex",
                    alignItems: "center",
                    justifyContent: "space-between",
                  }}
                >
                  <p
                    style={{
                      fontSize: 20,
                      fontStyle: "normal",
                      fontWeight: 400,
                    }}
                  >
                    Add Address
                  </p>

                  <p
                    style={{
                      color: "#4EA8DC",
                      cursor: "pointer",
                      fontSize: 14,
                      fontWeight: "normal",
                    }}
                    onClick={() =>
                      props.listAddress.length > 4
                        ? window.alert("Can't more than four field")
                        : props.setListAddress([
                            ...props.listAddress,
                            {
                              id: props.listAddress.length + 1,
                            },
                          ])
                    }
                  >
                    Add Another Address +
                  </p>
                </div>

                {props.listAddress.map((dataAddress, indexAddress) => (
                  <div key={indexAddress}>
                    <FieldText
                      fieldType="text"
                      onChange={(e) => props.handleChange(e)}
                      required
                      fieldName={`address${indexAddress}`}
                      label="Address *"
                      placeholder="Enter Address"
                    />
                    <FieldText
                      fieldType="text"
                      onChange={(e) => props.handleChange(e)}
                      required
                      fieldName={`postalCode${indexAddress}`}
                      label="Postal Code *"
                      placeholder="Enter Postal Code"
                    />

                    <div style={{ alignItems: "center", display: "flex" }}>
                      <input
                        onChange={(e) => props.handleChange(e)}
                        type="checkbox"
                        name="setDefaultAddress"
                      />
                      <label
                        style={{
                          color: "#9D9D9D",
                          fontSize: 13,
                          fontStyle: "normal",
                          fontWeight: 400,
                          marginLeft: 12,
                        }}
                      >
                        Make this default address
                      </label>
                    </div>

                    <div style={{ border: "1px solid #F1F1F1" }}></div>
                    <p
                      onClick={() => {
                        if (props.listAddress.length !== 1) {
                          let romveListAddress = props.listAddress.filter(
                            (filtering) => filtering.id !== dataAddress.id
                          );
                          props.setListAddress(romveListAddress);
                        }
                      }}
                      style={{
                        textAlign: "right",
                        marginTop: 8,
                        cursor: "pointer",
                      }}
                    >
                      X
                    </p>
                  </div>
                ))}
              </div>
            </div>

            <div
              style={{
                display: "flex",
                flexDirection: "row",
                justifyContent: "center",
              }}
            >
              <Button
                onClick={() => {
                  props.setListAddress([
                    {
                      id: 1,
                    },
                  ]);
                  props.setState(null);
                  props.setListAddress([
                    {
                      id: 1,
                    },
                  ]);
                  props.setListContactPerson([
                    {
                      id: 1,
                      listContactEmail: [{ id: 1 }],
                      listContactPhone: [{ id: 1 }],
                    },
                  ]);
                  props.setIsShowModal(false);
                }}
                secondary
                label="Cancel"
              />
              <Button submit label="Save" />
            </div>
          </div>
        </form>
      </div>
    )
  );
};

export default Modal;
