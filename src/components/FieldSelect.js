import React from "react";
import "./FieldSelect.css";

const FieldSelect = (props) => {
  return (
    <div className="form-control">
      <label>{props.label}</label>
      <select
        onChange={props.onChange}
        required={props.required}
        name={props.fieldName}
        placeholder="Enter client name"
      >
        {props.dataSource.map((data, index) => (
          <option value={data.value} key={index}>
            {data.label}
          </option>
        ))}
      </select>
    </div>
  );
};

export default FieldSelect;
